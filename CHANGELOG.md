# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.1 - 2024-01-31(11:40:31 +0000)

### Other

- Fix opensource sync

## Release v0.1.0 - 2023-01-24(10:04:33 +0000)

### New

- MoCa interfaces must be synchronised with NetModel

